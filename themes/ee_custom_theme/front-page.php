<?php /* Template Name: Inicio */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>
<?php get_header(); ?>
<main class="main-content home">
    <div class="c-main-banner">
        <div class="c-banner-text">
            <h1>"En Editorial Época un lector <br><span>vive mil vidas antes de morir"</span> </h1>
            <a href="<?php echo home_url(); ?>/ediciones" aria-label="Página de ediciones"><i class="fas fa-books"></i> Descubre nuestros
                ejemplares</a>
        </div>
        <div class="c-banner-image">
            <img src="<?php uri("image") ?>home/hombre-en-libros.svg" alt="">
        </div>
    </div>
    <div class="c-lasted-added">
        <div class="c-title">Agregados recientemente</div>
        <div class="c-lasted-aded-slider">
            <?php echo do_shortcode('[wpb-product-slider orderby="date" order="DESC"]'); ?>
        </div>
    </div>
    <div class="c-lasted-added hidden">
        <div class="c-title">Más visto</div>
        <div class="clasted-aded-slider">
            <?php echo do_shortcode('[product_categories number="0" parent="0"]'); ?>
        </div>
    </div>
    <div class="c-about-us">
        <div class="c-title">
            <h2>¿Por qué elegirnos? Conócenos</h2>
        </div>
        <div class="c-items-about-us">
            <div class="about-us-item">
                <img src="<?php uri("image") ?>home/extra-info/reloj-de-arena.svg" alt="">
                <h3>Tenemos una amplia experiencia en el mercado</h3>
            </div>
            <div class="about-us-item">
                <img src="<?php uri("image") ?>home/extra-info/libro.svg" alt="">
                <h3>Contamos con un catálogo muy extenso</h3>
            </div>
            <div class="about-us-item">
                <img src="<?php uri("image") ?>home/extra-info/evolucion.svg" alt="">
                <h3>Hacemos crecer nuestros catálogos constantemente</h3>
            </div>
            <div class="about-us-item">
                <img src="<?php uri("image") ?>home/extra-info/mundo.svg" alt="">
                <h3>Contamos con envíos nacionales y al extranjero</h3>
            </div>
        </div>
    </div>
    <div class="c-owr-clients">
        <div class="c-title">
            <h2>Nuestros clientes</h2>
        </div>
        <div class="owr-clients-items">
            <div class="owr-clients-item"><img src="<?php uri("image") ?>home/clients/logo-gandhi.svg" alt=""></div>
            <div class="owr-clients-item"><img src="<?php uri("image") ?>home/clients/logo-el-sotano.webp" alt=""></div>
            <div class="owr-clients-item"><img src="<?php uri("image") ?>home/clients/logo-walmart.svg" alt=""></div>
            <div class="owr-clients-item"><img src="<?php uri("image") ?>home/clients/logo-gonvill.svg" alt=""></div>
            <div class="owr-clients-item"><img src="<?php uri("image") ?>home/clients/logo-tony.svg" alt=""></div>
            <div class="owr-clients-item"><img src="<?php uri("image") ?>home/clients/logo-chedraui.svg" alt=""></div>
        </div>
    </div>
    <div class="c-distribuidores">
        <div class="c-distribuidroes-info">
            <p>La lectura está en todas partes pero no en todos esta el gusto por la lectura,
                si eres de los pocos a los que les encanta el olor de un libro nuevo te invitamos 
                a llenarte de experiencias que te envolverán en un mundo fantástico</p>
            <a href="<?php echo home_url(); ?>/nuestros-distribuidores" aria-label="Pagina de nuestros distribuidores"><i class="fas fa-user-check"></i> Visita nuestros
                distribuidores</a>
        </div>
        <div class="c-distribuidores-image">
            <img src="<?php uri("image") ?>home/mujer-negocios.webp" alt="">
        </div>
    </div>
</main>
<?php get_footer(); ?>