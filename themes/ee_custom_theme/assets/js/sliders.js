// Slider for banner in ediciones
jQuery(".c-slider").slick({
    autoplay: true,
    lazyLoad: "ondemand",
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    speed: 300,
    arrows: true,
    prevArrow: $(".prev"),
    nextArrow: $(".next"),
  });