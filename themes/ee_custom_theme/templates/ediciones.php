<?php /* Template Name: Ediciones */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>

<?php get_header(); ?>
<main class="main-content ediciones">
    <div class="c-ediciones-slider-main c-slider hidden">
        <div class="c-slide-item">
            <img data-lazy="<?php uri("image") ?>ediciones/slider/banner_1.png" alt="">
        </div>
        <div class="c-slide-item">
            <img data-lazy="<?php uri("image") ?>ediciones/slider/banner_2.png" alt="">
        </div>
        <div class="c-slide-item">
            <img data-lazy="<?php uri("image") ?>ediciones/slider/banner_3.png" alt="">
        </div>
    </div>
    <div class="c-ediciones-main">
        <div class="c-title">
            <h1>Todas nuestras ediciones</h1>
        </div>
        <div class="ediciones-items">
            <div class="ediciones-item">
                <img src="<?php uri("image") ?>ediciones/caratulas-ediciones/editorial-epoca.svg" alt="">
                <div class="info">
                    <h2>Ediorial Época</h2>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloremque blanditiis deserunt fugit
                        totam illo sunt molestias magni molestiae illum eum! Dolorum adipisci amet quo officiis ad
                        voluptatibus non ex unde.</p>
                    <a href="<?php echo home_url(); ?>/ediciones/editorial-epoca/" aria-label="Pagina hacia ediciones de la editorial"><i class="fas fa-info-circle"></i> Más información</a>
                </div>
            </div>
            <div class="ediciones-item">
                <img src="<?php uri("image") ?>ediciones/caratulas-ediciones/horus.svg" alt="">
                <div class="info">
                    <h2>HORUS</h2>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloremque blanditiis deserunt fugit
                        totam illo sunt molestias magni molestiae illum eum! Dolorum adipisci amet quo officiis ad
                        voluptatibus non ex unde.</p>
                    <a href="<?php echo home_url(); ?>/ediciones/horus" aria-label="Pagina hacia ediciones HORUS"><i class="fas fa-info-circle"></i> Más información</a>
                </div>
            </div>
            <div class="ediciones-item">
                <img src="<?php uri("image") ?>ediciones/caratulas-ediciones/apuntes-escolares.svg" alt="">
                <div class="info">
                    <h2>Apuntes Escolares</h2>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloremque blanditiis deserunt fugit
                        totam illo sunt molestias magni molestiae illum eum! Dolorum adipisci amet quo officiis ad
                        voluptatibus non ex unde.</p>
                    <a href="<?php echo home_url(); ?>/ediciones/apuntes-escolares" aria-label="Pagina hacia Apuntes Escolares"><i class="fas fa-info-circle"></i> Más información</a>
                </div>
            </div>
            <div class="ediciones-item">
                <img src="<?php uri("image") ?>ediciones/caratulas-ediciones/clasicos-infantiles.svg" alt="">
                <div class="info">
                    <h2>Clásicos Infantiles</h2>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloremque blanditiis deserunt fugit
                        totam illo sunt molestias magni molestiae illum eum! Dolorum adipisci amet quo officiis ad
                        voluptatibus non ex unde.</p>
                    <a href="<?php echo home_url(); ?>/ediciones/clasicos-infantiles" aria-label="Pagina hacia Clásicos Infantiles"><i class="fas fa-info-circle"></i> Más información</a>
                </div>
            </div>
            <div class="ediciones-item">
                <img src="<?php uri("image") ?>ediciones/caratulas-ediciones/nuevo-talento.svg" alt="">
                <div class="info">
                    <h2>Nuevo Talento</h2>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloremque blanditiis deserunt fugit
                        totam illo sunt molestias magni molestiae illum eum! Dolorum adipisci amet quo officiis ad
                        voluptatibus non ex unde.</p>
                    <a href="<?php echo home_url(); ?>/ediciones/nuevo-talento" aria-label="Pagina hacia Nuevo Talento"><i class="fas fa-info-circle"></i> Más información</a>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>