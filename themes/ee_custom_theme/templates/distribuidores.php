<?php /* Template Name: Nuestros distribuidores */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>
<?php get_header(); ?>
<main class="main-content distribuidores">
    <div class="c-distribuidores">
        <div class="distribuidores-header">
            <div class="distribuidores-header-elements">
                <h1>Nuestros distribuidores</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Esse veniam provident, doloremque quam aliquid laborum beatae
                    laudantium iure exercitationem ipsa, impedit, facilis suscipit?
                    Facere, nobis cumque tempora cupiditate ad sint!</p>
            </div>
        </div>
        <div class="c-distribuidores-items">
        </div>
    </div>
</main>
<script>
    var distribuidoresArray = [{
            "nombreEditorial": "EDITORIAL IZTACCIHUATL DE MONTERREY, S.A. DE C.V.",
            "direccion": "QUIMICOS # 120 COL. TECNOLOGICO C.P. 64700, MONTERREY, N.L.",
            "linkMaps": "https://www.google.com.mx/maps/place/Librer%C3%ADas+Iztacc%C3%ADhuatl/@25.6492121,-100.296471,17z/data=!3m1!4b1!4m5!3m4!1s0x8662bfbe92891cf7:0xbdee4885465a2fe2!8m2!3d25.6492292!4d-100.2943016",
            "phoneNumber": ""
        },
        {
            "nombreEditorial": "LIBRERÍA MADERO DE MORELIA, S.A. DE C.V.",
            "direccion": "MADERO ORIENTE NO. 338-A COL. CENTRO, C.P. 58000 MORELIA, MICH.",
            "linkMaps": "https://www.google.com.mx/maps/place/Libreria+de+libros+Madero+De+Morelia+Sa+De+Cv/@19.7027315,-101.1904483,17z/data=!3m1!4b1!4m5!3m4!1s0x842d0e72494b5831:0x92fff9ba2f1e3e83!8m2!3d19.7027584!4d-101.1882584",
            "phoneNumber": "014433121609"
        },
        {
            "nombreEditorial": "LIBERIA LA CATEDRAL",
            "direccion": "HIDALGO No. 132 COL. CENTRO 37000, LEON. GTO.",
            "linkMaps": "https://www.google.com.mx/maps/place/Librer%C3%ADa+Catedral+de+Le%C3%B3n/@21.1231873,-101.68458,17z/data=!3m2!4b1!5s0x842bbf0c976c662b:0xdd0f724cbd70e625!4m5!3m4!1s0x842bbf0c9837b1a1:0xade1807c077ccee0!8m2!3d21.1231823!4d-101.6823913",
            "phoneNumber": ""
        },
        {
            "nombreEditorial": "JOSE LOPEZ TEJERA / LIB. CIENTIFICA",
            "direccion": "AVE. 20 DE NOVIEMBRE #558 COL. CENTRO, C.P. 91700 VERACRUZ, VERACRUZ",
            "linkMaps": "https://www.google.com.mx/maps/place/Av+20+de+Noviembre+558,+Salvador+D%C3%ADaz+Mir%C3%B3n,+91700+Veracruz,+Ver./@19.1896821,-96.1353586,17z/data=!3m1!4b1!4m5!3m4!1s0x85c346b64b3afa2d:0x8c2f1d5425bb857d!8m2!3d19.189677!4d-96.1331699",
            "phoneNumber": "2299329053"
        },
        {
            "nombreEditorial": "LIBRERIAS DANTE, S.A. DE C.V.",
            "direccion": "CALLE 19 Nº 102 x 20 COL. MEXICO 97125, MERIDA, YUCATAN",
            "linkMaps": "https://www.google.com.mx/maps/place/Librer%C3%ADa+Dante+20+x+21/@20.9901386,-89.6354287,13z/data=!4m9!1m2!2m1!1sLIBRERIAS+DANTE,+S.A.+DE+C.V.!3m5!1s0x8f5677902f1e45cf:0x31de26fe5ad5efa0!8m2!3d21.002915!4d-89.610665!15sCh1MSUJSRVJJQVMgREFOVEUsIFMuQS4gREUgQy5WLiIDiAEBWhoiGGxpYnJlcmlhcyBkYW50ZSBzYSBkZSBjdpIBCmJvb2tfc3RvcmU",
            "phoneNumber": "9999441985"
        },
        {
            "nombreEditorial": "EL ESCRITORIO MODERNO, S.A. DE C.V.",
            "direccion": "3ª AVENIDA SUR ORIENTE # 748 COL. CENTRO 29000, TUXTLA GTZ, CHIAPAS",
            "linkMaps": "https://www.google.com.mx/maps/place/3a+Avenida+Sur+Ote.+748,+Asamblea+de+barrio,+San+Roque,+29000+Tuxtla+Guti%C3%A9rrez,+Chis./@16.7501037,-93.1132653,17z/data=!3m1!4b1!4m5!3m4!1s0x85ecd889a718d12d:0xca6f224388ec04d!8m2!3d16.7500986!4d-93.1110766",
            "phoneNumber": ""
        },
        {
            "nombreEditorial": "JUAN COLORADO CARDONA O MI LIBRERÍA",
            "direccion": "5 DE MAYO # 255 COL. CENTRO 78000, SAN LUIS POTOSI",
            "linkMaps": "https://www.google.com.mx/maps/place/Mi+Librer%C3%ADa/@22.1503425,-100.9786543,17z/data=!3m2!4b1!5s0x842aa20197c251a9:0xd95184e942a2f5b5!4m5!3m4!1s0x842aa201a2969cc9:0x5a3b30728a5253f8!8m2!3d22.1503375!4d-100.9764656",
            "phoneNumber": ""
        },
        {
            "nombreEditorial": "LIB. UNIVERSAL DE ZACATECAS",
            "direccion": "HIDALGO Nº 109 COL.CENTRO 98000, ZACATECAS, ZACATECAS",
            "linkMaps": "https://www.google.com.mx/maps/place/LIBRER%C3%8DA+UNIVERSAL/@22.7724514,-102.5767203,17z/data=!3m1!4b1!4m5!3m4!1s0x86824e88390f3ad1:0x9908cf740ea9f11b!8m2!3d22.7724429!4d-102.5745235",
            "phoneNumber": "014929241240"
        },
        {
            "nombreEditorial": "Pasaje Zócalo-Pino Suárez",
            "direccion": "Metro de la Ciudad de México Local 29",
            "linkMaps": "https://www.google.com.mx/maps/place/P.za+de+la+Constituci%C3%B3n+%26+Jos%C3%A9+Mar%C3%ADa+Pino+Su%C3%A1rez,+Centro+Hist%C3%B3rico+de+la+Cdad.+de+M%C3%A9xico,+Centro,+06060+Ciudad+de+M%C3%A9xico,+CDMX/@19.4332763,-99.1330565,18.63z/data=!4m5!3m4!1s0x85d1fecd32f5a033:0x10f58a758099c7a8!8m2!3d19.4332972!4d-99.1324443",
            "phoneNumber": "5555223130"
        },
        {
            "nombreEditorial": "EDICIONES EPOCA",
            "direccion": "Allende # 14 Col. Centro, Esq. Donceles Ciudad de México",
            "linkMaps": "https://www.google.com.mx/maps/place/Ignacio+Allende+14,+Centro+Hist%C3%B3rico+de+la+Cdad.+de+M%C3%A9xico,+Centro,+Cuauht%C3%A9moc,+06000+Ciudad+de+M%C3%A9xico,+CDMX/@19.4369891,-99.1400982,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1f92c4fbe8e55:0xb4a219515ad31a71!8m2!3d19.4369841!4d-99.1379095",
            "phoneNumber": "5555127605"
        }
    ]

    jQuery.each(distribuidoresArray, function(i, val) {
        var distribuidoresContainer = jQuery('<div class="item-card"></div>');
        var $element = distribuidoresContainer;
        $element.append(jQuery("<p class='editorial-name'>" + val.nombreEditorial + "</p>"));
        $element.append(jQuery("<p class='editorial-direction'><i class='fas fa-map-marker-alt'></i>" + val.direccion + "</p>"));

        $element.append(jQuery('<div class="c-distribuidores-cta" >'
            + '<a href=' + val.linkMaps + ' target="_blank" class="see-map" rel="noopener noreferrer">Ver Mapa'
            + '<i class="fas fa-external-link-alt"></i></a>'
            + '</div>'));
        distribuidoresContainer.append($element);
        jQuery(".c-distribuidores-items").append(distribuidoresContainer);
    });
</script>
<?php get_footer(); ?>