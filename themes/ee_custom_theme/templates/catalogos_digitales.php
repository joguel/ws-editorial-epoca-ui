<?php /* Template Name: Catálogos digitales */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>

<?php get_header(); ?>
<main class="main-content catalogos-digitales">
    Catálogos digitales

    <div class="c-catalogos-digitales-main">

        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-apuntes-escolares.webp" alt="">
                <div class="info">
                    <h2>Apuntes Escolares</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_AE_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" class="open-modal apuntesEscolares">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-clasicos-infantiles.webp" alt="">
                <div class="info">
                    <h2>Clásicos Infantiles</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_CI_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" class="open-modal clasicosInfantiles">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-diviertete-aprende.webp" alt="">
                <div class="info">
                    <h2>Diviertete y Aprende</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_DYA_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" class="open-modal divierteteAprende">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>    
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-editorial-epoca.webp" alt="">
                <div class="info">
                    <h2>Editorial Época</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_EE_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" class="open-modal editorialEpoca">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-horus.webp" alt="">
                <div class="info">
                    <h2>Ediciones HORUS</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_H_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" class="open-modal horus">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-nuevo-talento.webp" alt="">
                <div class="info">
                    <h2>Nuevo Talento</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_NT_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" class="open-modal nuevoTalento">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalogo-items">
            <div class="catalogo-item">
                <img src="<?php uri("image") ?>catalogos-digitales/catalogo-rtm.webp" alt="">
                <div class="info">
                    <h2>Ediciones RTM</h2>
                    <div class="c-buttons">
                        <a href="<?php uri("pdf") ?>catalogos/CATALOGO_RTM_2017_digital.pdf" download>
                            <i class="fas fa-download"></i></a>
                        <a href="#" class="open-modal RTM">
                            <i class="far fa-eye"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal_catalogo AE">
        <div class="modal-data">
            <button class="close-modal">X</button>
            <embed src="<?php uri('pdf') ?>catalogos/CATALOGO_AE_2017_digital.pdf" type="application/pdf" />
        </div>
    </div>
    <div class="modal_catalogo CI">
        <div class="modal-data">
            <button class="close-modal">X</button>
            <embed src="<?php uri('pdf') ?>catalogos/CATALOGO_CI_2017_digital.pdf" type="application/pdf" />
        </div>
    </div>
    <div class="modal_catalogo DYA">
        <div class="modal-data">
            <button class="close-modal">X</button>
            <embed src="<?php uri('pdf') ?>catalogos/CATALOGO_DYA_2017_digital.pdf" type="application/pdf" />
        </div>
    </div>
    <div class="modal_catalogo EE">
        <div class="modal-data">
            <button class="close-modal">X</button>
            <embed src="<?php uri('pdf') ?>catalogos/CATALOGO_EE_2017_digital.pdf" type="application/pdf" />
        </div>
    </div>
    <div class="modal_catalogo H">
        <div class="modal-data">
            <button class="close-modal">X</button>
            <embed src="<?php uri('pdf') ?>catalogos/CATALOGO_H_2017_digital.pdf" type="application/pdf" />
        </div>
    </div>
    <div class="modal_catalogo NT">
        <div class="modal-data">
            <button class="close-modal">X</button>
            <embed src="<?php uri('pdf') ?>catalogos/CATALOGO_NT_2017_digital.pdf" type="application/pdf" />
        </div>
    </div>
    <div class="modal_catalogo RTM">
        <div class="modal-data">
            <button class="close-modal">X</button>
            <embed src="<?php uri('pdf') ?>catalogos/CATALOGO_RTM_2017_digital.pdf" type="application/pdf" />
        </div>
    </div>
</main>
<?php get_footer(); ?>