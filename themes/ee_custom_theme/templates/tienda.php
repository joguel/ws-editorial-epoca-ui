<?php /* Template Name: Tienda - Principal */ ?>
<?php defined('ABSPATH') or die('No script kiddies please!'); ?>

<?php get_header(); ?>
<main class="main-content carrito">
    <?php echo do_shortcode('[products ids="all"]'); ?>
</main>
<?php get_footer(); ?>